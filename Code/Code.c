#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xil_printf.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "Xil_io.h"
#include "Xil_cache.h"
#include "stdbool.h"
#include "sleep.h"
#define PackLen	640*480
#define INPUT_HORIZON   800
#define INPUT_VERTICAL  600
#define OUTPUT_HORIZON  800
#define OUTPUT_VERTICAL 600
XScuGic intr_inst; // interrupt controller instance
XScuGic_Config *GicConfig; // interrupt controller config struct pointer


unsigned int *a=(unsigned int*)0x0D000000;// frame 1  start address.640*480=0x4B000.So second frame will be start at a+0x4b000
unsigned int *b=(unsigned int*)(0x0D000000 + 0x15F900);// frame 2  start address
unsigned int *c=(unsigned int*)(0x0D000000 + 0x15F900 + 0x15F900); // frame 3 start addr

unsigned char frame_addr_count=1;
unsigned char s2mm_addr_indc=1;
unsigned char mm2s_addr_indc=0;

unsigned char s2mm_intr_ok=0; // indicates if s2mm or mm2s recieved interrupt lately
unsigned char mm2s_intr_ok=0;
unsigned char control = 1; // first start


int vdma_s2mm_init(int,unsigned char);
int vdma_mm2s_init(int,unsigned char);
void vdma_s2mm_intr(void *);
void vdma_mm2s_intr(void *);
void write_read_control(void *);

void Interrupt_Flag_Control(){ // this will check if one of the dma's flagged an interrupt.s2mm 0x34 mm2s 0x04
	int reg_val;
	// first control the s2mm dma_Sr = 0x34
	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x34);
	reg_val = reg_val & 0x00001000;
	if(reg_val == 0x00001000){
	    // this means interrupt caused because s2mm.
		xil_printf("Interrupt occured because of s2mm..\n");
		reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x34);
		reg_val = reg_val | 0x00000800;
		//dma_init_store((unsigned int)1);
		s2mm_intr_ok = 1;
	}
	// second, control the mm2s dma_sr = 0x04
	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x04);
	reg_val = reg_val & 0x00001000;
	if(reg_val == 0x00001000){
		xil_printf("Interrupt occured because of mm2s..\n");
		//dma_init_get((unsigned int)1);
		mm2s_intr_ok = 1;
	}

}
//===================================================================================================================================

void write_read_control(void *CallBackRef){ // we assuming that both s2mm and mm2s will enter this interrupt service when their interrupt on complete bit is set.
	unsigned int reg_val;
	xil_printf("Main Interrupt!!! \n");
	Interrupt_Flag_Control();

	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x30);
	xil_printf("Vdma_cr_s2mm=%0x \n",reg_val);

	reg_val=Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x54);// horizontal
	xil_printf("Vdma_mm2s_hsize=%0x \n",reg_val);

	reg_val=Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x58);// stride and frm_delay(default as 1 so we didnt touch it
	xil_printf("Vdma_mm2s_stride=%0x \n",reg_val);

	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x34);
	xil_printf("Vdma_sr_s2mm=%0x \n",reg_val);

}


//====================================================================================================================================
int SetUpInterruptSystem(XScuGic *XScuGicInstancePtr){ // interrupt setting function.
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler)XScuGic_InterruptHandler,XScuGicInstancePtr);
	Xil_ExceptionEnable();
	return XST_SUCCESS;
}
//====================================================================================================================================

//===================================================================================================================================

int vdma_init(void){ // both s2mm and mm2s and genlock configurations in here
	// first of all reset the vdma
	unsigned int reg_val;

	// first of all we need to configure s2mm side(genlock master)  s2mm_cr = 0x30h 0x00005009

	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x30);
	reg_val = reg_val | 0x0000d009; //  Err_IrqEn FrmCnt_IrqEn repeat_frame_on_error GenlockEn RS (genlock ignored as channel is master.)
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x30,reg_val);

	// s2mm horizontal and stride configuration
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xA4,INPUT_HORIZON*3);// horizontal
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xA8,INPUT_HORIZON*3);// stride and frm_delay(default as 1 so we didnt touch it

	// now s2mm start adress assignment offset 0xAC
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xAC,0x0D000000);// first addresss buffer
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xAC + 4,0x0D000000 + 0x15F900);// second addresss buffer
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xAC + 8,0x0D000000 + 0x15F900 + 0x15F900);// third addresss buffer

	// ==========================================now mm2s.its configured as master =================================================
	reg_val = Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x00);
	reg_val = reg_val | 0x00005009; //  Err_IrqEn FrmCnt_IrqEn GenlockEn RS (genlock accepted channel is master.)
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x00,reg_val);

	// mm2s horizontal and stride config
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x54,OUTPUT_HORIZON*3);// horizontal
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x58,OUTPUT_HORIZON*3);// stride and frm_delay(default as 1 so we didnt touch it

	// mm2s start addresss buffer offset 0x5c
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x5C,0x0D000000);// first addresss buffer
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x5C + 4,0x0D000000 + 0x15F900);// second addresss buffer
	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x5C + 8,0x0D000000 + 0x15F900 + 0x15F900);// third addresss buffer

	return 0;
}
//======================================================================================================================


//======================================================================================================================
/*
 * InitializeInterruptSystem(u16 deviceID)
 * What it does?
 *
 * 1-) Gets look up config and verifies it.
 * 2-) Test itselfs
 * 3-)Starts SetUpInterruptSystem() function
 * 4-) If something is wrong, return XST_FAILURE else returns XST_SUCCESS
 */

int InitializeInterruptSystem(u16 deviceID){
	int status;

	GicConfig = XScuGic_LookupConfig( deviceID );
	if(GicConfig == NULL){
		return XST_FAILURE;
	}
	status = XScuGic_CfgInitialize(&intr_inst,GicConfig,GicConfig->CpuBaseAddress);
	if(status != XST_SUCCESS){
		return XST_FAILURE;
	}
	status = XScuGic_SelfTest(&intr_inst);
	if (status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	status=SetUpInterruptSystem(&intr_inst);
	if(status != XST_SUCCESS){
	    return XST_FAILURE;
	}
	// s2mm ve mm2s need to connect same interrupt routine
/*
    status=XScuGic_Connect(&intr_inst,XPAR_FABRIC_AXI_VDMA_0_S2MM_INTROUT_INTR,(Xil_ExceptionHandler)write_read_control,(void *)&intr_inst);
    if(status != XST_SUCCESS){
    	return XST_FAILURE;
    }
    XScuGic_Enable(&intr_inst, XPAR_FABRIC_AXI_VDMA_0_S2MM_INTROUT_INTR);// interrupt config
*/
    status=XScuGic_Connect(&intr_inst,XPAR_FABRIC_AXI_VDMA_0_MM2S_INTROUT_INTR,(Xil_ExceptionHandler)write_read_control,(void *)&intr_inst);
    if(status != XST_SUCCESS){
    	return XST_FAILURE;
    }
    XScuGic_Enable(&intr_inst, XPAR_FABRIC_AXI_VDMA_0_MM2S_INTROUT_INTR);// interrupt config

    return XST_SUCCESS;//

}


/*
 * dma_init_store(unsigned int caller)
 * what it does?
 *
 * 1-) This function is main operation block both initialization and resetting the dma.
 * 2-) caller=0 means dma initialization about the occur.
 * 3-) caller=1 means dma interrupt needs to reset dma before begin another transaction.
 *
 */


int main()
{
    init_platform();
    unsigned int reg_val;
    int status;
    xil_printf("Start of the program !!!!\n");
    vdma_init();// configures both channel and genlock but not started yet
    Xil_DCacheDisable(); // disable the cache and dma directly access to the ddr.

    // so far we initialized platform,ps7_init,post config and dma interrupt,address config has done////
    // initializing interrupt system
    status=InitializeInterruptSystem(XPAR_SCUGIC_0_DEVICE_ID);
    if(status !=XST_SUCCESS){
    	xil_printf("Interrupt Initialization Has Been Failed!");
    	return -1;
    }

    xil_printf("To start the first loop, press a button...\n");
    getchar();

	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0x50,OUTPUT_VERTICAL);// this code starts the vdma_mm2s transaction
	reg_val=Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0x50);
	if( reg_val != OUTPUT_VERTICAL){
		xil_printf("mm2s_vsync config failed!! \n");
		return -1;
	}

	Xil_Out32(XPAR_AXI_VDMA_0_BASEADDR + 0xA0,INPUT_VERTICAL);// this code starts the vdma_s2mm transaction
	reg_val=Xil_In32(XPAR_AXI_VDMA_0_BASEADDR + 0xA0);
	if( reg_val != INPUT_VERTICAL){
		xil_printf("s2mm_vsync config failed!! \n");
		return -1;
	}


    cleanup_platform();
    return 0;
}
