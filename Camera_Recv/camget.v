`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/28/2022 03:00:07 PM
// Design Name: 
// Module Name: camget
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module camget(
// axi side control signals
    input wire axi_aclk,
    input wire axi_aresetn,
    output wire [23:0] m_axis_tdata,
    output wire [0:0]  m_axis_tdest,
    output wire [0:0]  m_axis_tid,
    output wire [2:0]  m_axis_tkeep,
    output wire [0:0]  m_axis_tlast,
    input  wire        m_axis_tready,
    //output wire [2:0]  m_axis_tstrb,
    output wire [0:0]  m_axis_tuser,
    output wire        m_axis_tvalid,
// camera side control signals
    input wire       PCLK,
    input wire       HS,
    input wire       VS,
    input wire [7:0] D
    );
    
    // common registers between cam logic and axi logic 
    reg [24:0] line_buf_z [799:0]; // 24'b0_8'bu_reg_8'by_reg_8'bv_reg    24'�nc� indicates sof tuser value.
    reg [24:0] line_buf_o [799:0]; //  24'b0_8'bu_reg_8'by_reg_8'bv_reg    24'�nc� bit sof'yi temsil ediyor.
    reg        line_buf_reg = 1'b0; // 0 means line_buf_z and 1 means line_buf_o
    
    // camera control logic 
    parameter CAM_IDLE = 2'd0,SOF = 2'd2,GET_LINE = 2'd3; // GET_LINE is missing
    reg [2:0] cam_state = CAM_IDLE;
    reg [7:0] u_reg     = 8'd0;
    reg [7:0] yz_reg    = 8'd0;
    reg [7:0] v_reg     = 8'd0;
    reg [7:0] yo_reg    = 8'd0;
    reg [1:0] part      = 2'b00;
    reg [9:0] pix_count = 10'd0;
    reg [9:0] line_count = 10'd0;
    reg [9:0] axi_line_count = 10'd0;
    reg       start_frm = 1'b0;
    reg       send_line_buf_z = 1'b0;
    reg       send_line_buf_o = 1'b0;
    
    // axi control logic
    parameter AXI_IDLE = 2'd0,SEND_BUF_Z = 2'd1,SEND_BUF_O = 2'd2,END_FRAME = 2'd3;
    reg [1:0] axi_state = AXI_IDLE;
    reg [9:0] axi_pix_counter = 10'd0;
    always @(posedge axi_aclk)begin
        if(!axi_aresetn)begin
            axi_state <= AXI_IDLE;
        end
        else begin
            case(axi_state)
                AXI_IDLE:begin
                    axi_state <= (send_line_buf_z)?SEND_BUF_Z:( (send_line_buf_o)?SEND_BUF_O:AXI_IDLE );
                end
                SEND_BUF_Z:begin
                    if(m_axis_tvalid && m_axis_tready) begin
                        axi_pix_counter <= ((axi_pix_counter + 1'b1) == 10'd800)?10'd0:(axi_pix_counter + 1'b1);
                        if((axi_pix_counter + 1'b1) == 10'd800)begin
                            axi_line_count  <= axi_line_count + 1'b1;
                            axi_state       <= ((axi_line_count+1'b1) == 10'd478)?END_FRAME:AXI_IDLE;
                        end
                    end 
                end
                SEND_BUF_O:begin
                    if(m_axis_tvalid && m_axis_tready) begin
                        axi_pix_counter <= ((axi_pix_counter + 1'b1) == 10'd800)?10'd0:(axi_pix_counter + 1'b1);
                       if((axi_pix_counter + 1'b1) == 10'd800)begin
                             axi_line_count  <= axi_line_count + 1'b1;
                             axi_state       <= ((axi_line_count+1'b1) == 10'd478)?END_FRAME:AXI_IDLE;
                       end
                    end 
                end
                END_FRAME:begin
                    if(m_axis_tvalid && m_axis_tready)begin
                        axi_pix_counter <= ((axi_pix_counter + 1'b1) == 10'd800)?10'd0:(axi_pix_counter + 1'b1);
                        if((axi_pix_counter + 1'b1) == 10'd800)begin
                            axi_line_count  <= ((axi_line_count+1'b1) == 10'd600)?10'd0:(axi_line_count + 1'b1);
                            axi_state       <= ((axi_line_count+1'b1) == 10'd600)?AXI_IDLE:END_FRAME;
                        end
                    end
                end
                default:begin
                    axi_state <= AXI_IDLE;
                end
            endcase
        end
    end   
    
    assign m_axis_tvalid = (axi_state != AXI_IDLE);
    assign m_axis_tdata  = (axi_pix_counter < 10'd640)?( (axi_state == SEND_BUF_Z)?line_buf_z[axi_pix_counter][23:0]:((axi_state == SEND_BUF_O)?line_buf_o[axi_pix_counter][23:0]:24'd0)):24'd0;
    assign m_axis_tuser  = (axi_pix_counter < 10'd640)?( (axi_state == SEND_BUF_Z)?line_buf_z[axi_pix_counter][24]:((axi_state == SEND_BUF_O)?line_buf_o[axi_pix_counter][24]:1'b0)):1'b0;
    assign m_axis_tlast  = (axi_pix_counter == 10'd799);
    assign m_axis_tid    = 0;
    assign m_axis_tdest  = 0;
    assign m_axis_tkeep  = 3'b111;
    //assign m_axis_tstrb  = 3'b111;
     
    always@(posedge PCLK)begin
        if(!axi_aresetn)begin
            // reset lock
            u_reg     <= 8'd0;
            yz_reg    <= 8'd0;
            v_reg     <= 8'd0;
            yo_reg    <= 8'd0;
            part      <= 2'd0;
            pix_count <= 10'd0;
            
        end
        else begin
            case(cam_state)
                CAM_IDLE:begin
                    cam_state <= (VS)?SOF:CAM_IDLE;
                end
                SOF:begin
                    cam_state       <= (!VS)?GET_LINE:SOF;
                    start_frm       <= (!VS);
                    part            <= 2'd0;
                    u_reg           <= 8'd0;
                    yz_reg          <= 8'd0;
                    v_reg           <= 8'd0;
                    pix_count       <= 10'd0;
                    line_buf_reg    <= 1'b0;
                    send_line_buf_z <= 1'b0;
                    send_line_buf_o <= 1'b0;
                    
                end
                GET_LINE:begin
                    cam_state <= (VS)?SOF:GET_LINE;
                    if(HS)begin
                        case(part)
                            2'd0:begin
                                u_reg <= D[7:0];
                                part  <= part + 1'b1;
                            end
                            2'd1:begin
                                yz_reg <= D[7:0];
                                part   <= part + 1'b1; 
                            end
                            2'd2:begin
                                v_reg     <= D[7:0];
                                part      <= part + 1'b1;
                                pix_count <= pix_count + 1'b1; 
                            end
                            2'd3:begin
                                case(line_buf_reg)
                                    1'b0:begin
                                        line_buf_z[pix_count - 1'b1] <= {start_frm,u_reg,yz_reg,v_reg};
                                        line_buf_z[pix_count       ] <= {1'b0     ,u_reg,D[7:0],v_reg}; 
                                        send_line_buf_z <= ((pix_count + 1'b1) == 640);
                                        send_line_buf_o <= 1'b0;
                                    end
                                    1'b1:begin
                                        line_buf_o[pix_count - 1'b1] <= {1'b0,u_reg,yz_reg,v_reg};
                                        line_buf_o[pix_count       ] <= {1'b0,u_reg,D[7:0],v_reg}; 
                                        send_line_buf_o <= ((pix_count + 1'b1) == 640);
                                        send_line_buf_z <= 1'b0;
                                    end
                                endcase
                                line_buf_reg <= ((pix_count + 1'b1) == 640)?(line_buf_reg + 1'b1):line_buf_reg;
                                start_frm  <= 1'b0; 
                                part       <= part + 1'b1;
                                pix_count  <= ((pix_count + 1'b1) == 640)?10'd0:(pix_count + 1'b1);
                                line_count <= ((pix_count + 1'b1) == 640)?(line_count + 1'b1):line_count;
                            end
                            default:begin
                                // error logic
                            end
                        endcase
                    end
                end
            endcase 
        end
    end
endmodule
