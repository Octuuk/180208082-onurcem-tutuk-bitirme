`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/20/2022 03:39:29 PM
// Design Name: 
// Module Name: axilitesccb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module axilitesccb(
    
    // global signals
    input wire axi_aclk,
    input wire axi_arstn,
    // Write addr signals
    input  wire [31:0] s_axi_awaddr, // axi4lite2sccb slave address.
    input  wire        s_axi_awvalid,
    output wire        s_axi_awready,
    // Write data signals 
    input  wire [31:0] s_axi_wdata, // [31:24] = Write addr of ov7670 , [23:16] = Subaddr of ov7670 reg, [15:8] = data of reg , [7:0] = config vars
    input  wire [3:0]  s_axi_wstrb,
    input  wire        s_axi_wvalid,
    output wire        s_axi_wready,
    // Write response signals
    output wire [1:0]  s_axi_bresp,
    output wire        s_axi_bvalid,
    input  wire        s_axi_bready,
    // Read addr signals
    input  wire [31:0] s_axi_araddr,// axi4lite2sccb slave address.Burst operation not supported so master needs to issue 200 op for each of the register read ops.
    input  wire        s_axi_arvalid,
    output wire        s_axi_arready,
    // Read data signals
    output wire [31:0] s_axi_rdata, // [31:24] = subaddr of reg , [23:16] = not care , [15:8] = not care, [7:0] = reg val 
    output wire [1:0]  s_axi_rresp, // if there is an error during sccb interface while acquiring the reg val, return slverr else return okay response
    output wire        s_axi_rvalid,
    input  wire        s_axi_rready,
    // SCCB interface 
    output wire SCL,
    inout       SDA,
    // Kamera signals
    output wire PWDN,
    output wire RST  
    );
    
// data inside each of these registers are stored and send with msb fashion. 7....0 --------   -7(first bit to send)-6-5-4-3-2-1-0-x(last bit to send.)-
reg [8:0] wr_id_address  = {7'h43,1'b0,1'b0}; // 0x43 write address, 1'b0 means write operation and last 1'b0 is not care bit
reg [8:0] rd_id_address  = {7'h42,1'b1,1'b0}; // 0x42 read address, 1'b1 means read operation and last 1'b0 is not care bit
reg [8:0] sub_address = 9'b101010101; // specific address of register.
reg [9:0] write_data  = 10'b1010101000; // data array which will be used for tranasction    
reg [8:0] read_data   = 9'b0; // data which will be acquired from ov7670 will be stored here for further process   
reg [9:0] read_phs    = 10'bzzzzzzzz10;

// SCL clock controller ===================BEGINS===========================
reg       sck_reg = 1'b1;
reg       sda_reg = 1'b1;
parameter CLK_IDLE=1'b0,CLK_START=1'b1;
parameter CLK_HUNDRED_KHZ=500;
reg [8:0] clock_gen_count = 0;
reg       clk_state = CLK_IDLE;
reg       sck_start  = 1'b0;
always @(posedge axi_aclk)begin
    if(!axi_arstn)begin
        clk_state <= CLK_IDLE;
    end
    else begin
        case(clk_state)
            CLK_IDLE:begin
                sck_reg         <= (sck_start)?1'b0:1'b1;
                clk_state       <= (sck_start)?CLK_START:CLK_IDLE;
                clock_gen_count <= 0;
            end
            CLK_START:begin
                // scl clock control logic
                sck_reg         <= (clock_gen_count == CLK_HUNDRED_KHZ)?(!sck_reg):sck_reg;
                clock_gen_count <= (clock_gen_count == CLK_HUNDRED_KHZ)?9'd0:(clock_gen_count+9'd1); 
                clk_state       <=  (sck_start)?CLK_START:CLK_IDLE;
            end
        endcase
    end
end
// SCL clock controller ====================ENDS============================
parameter PREDEF_CLK_DELAY = 9'd200;
parameter IDLE=3'd0,START=3'd1,DELAY=3'd2,DROP_SCL=3'd3,SEND=3'd4,HIGH_SCL=3'd5,LOW_SCL=3'd6,HIGH_SDA=3'd7;
parameter PHASE_ONE = 3'd1,PHASE_TWO=3'd2,PHASE_THREE=3'd3,PHASE_FOUR=3'd4,PHASE_FIVE=3'd5;
reg [2:0] sccb_state = IDLE;
reg [2:0] sccb_next_state = IDLE;
reg [2:0] phase_var = 3'd1; // 1 means phase1, 2 means phase2 ..... 5 means phase5
reg [2:0] phase_index = 0;
reg [8:0] clk_count = 0;
reg [3:0] count_bit = 4'd8;
reg       sccb_start = 1'b0; //means that sccb will start if given reg is high.It will be controlled with sccb block
reg       axi_done   = 1'b0; // means that axi side transaction acquired and ready for sccb transaction.Controlled by axi side block
reg       read_op_inprogress = 1'b0;
reg       write_op_inprogress = 1'b0;
// axi side operations ====================BEGINS===========================
parameter AXI_IDLE=3'd0,GET_AWADDR=3'd1,GET_WDATA=3'd2,START_SCCB=3'd3,GET_ARADDR=3'd4,SEND_BRESP=3'd5,SEND_RRESP=3'd6,RESET=3'd7;
reg [2:0] axi_state      = AXI_IDLE;
reg [2:0] axi_next_state = AXI_IDLE;
reg [7:0] reg_count      = 8'd0;
reg [9:0] reset_count    = 10'd1;
reg [8:0] reset_addr     = {8'hff,1'b0};

assign s_axi_awready = ( (axi_state == GET_AWADDR) && (sccb_state == IDLE) )?1'b1:1'b0;
assign s_axi_wready  = (axi_state == GET_WDATA)?1'b1:1'b0;
assign s_axi_bvalid  = (axi_state == SEND_BRESP)?1'b1:1'b0;
assign s_axi_bresp   = 2'b00;

assign s_axi_arready = ( (axi_state == GET_ARADDR) && (sccb_state == IDLE) )?1'b1:1'b0;
assign s_axi_rdata   = {reg_count,16'b0,read_data[8:1]};
assign s_axi_rvalid  = (axi_state == SEND_RRESP)?1'b1:1'b0;
assign s_axi_rresp   = 2'b00;

always@(posedge axi_aclk)begin
    if(!axi_arstn)begin
        axi_state      <= AXI_IDLE;
        axi_next_state <= AXI_IDLE; 
    end
    else begin
        case(axi_state)
            AXI_IDLE:begin
                axi_state           <= (s_axi_awvalid)?GET_AWADDR:( (s_axi_arvalid)?GET_ARADDR:AXI_IDLE );
                read_op_inprogress  <= 1'b0;
                write_op_inprogress <= 1'b0;  
                axi_done            <= 1'b0;
            end
            GET_AWADDR:begin
                write_op_inprogress <= 1'b1;
                sub_address         <= {s_axi_awaddr[7:0],1'b0};
                axi_state           <= (s_axi_awvalid && s_axi_awready)?GET_WDATA:GET_AWADDR; 
            end
            GET_WDATA:begin
                write_data     <= {s_axi_wdata[7:0],2'b0};
                axi_state      <= (s_axi_wready && s_axi_wvalid)?((reset_addr != sub_address)?START_SCCB:RESET):GET_WDATA;
                axi_next_state <= SEND_BRESP;  
            end
            START_SCCB:begin
                axi_done  <= 1'b1;
                axi_state <= (sccb_start)?axi_state:axi_next_state;
            end
            SEND_BRESP:begin
                axi_done  <= 1'b0;
                axi_state <= (s_axi_bvalid && s_axi_bready)?AXI_IDLE:SEND_BRESP; 
            end
            GET_ARADDR:begin
                read_op_inprogress <= 1'b1;
                sub_address        <= {s_axi_araddr[7:0],1'b0};
                axi_state          <= (s_axi_arvalid && s_axi_arready)?START_SCCB:GET_ARADDR;
                axi_next_state     <= SEND_RRESP;
            end
            SEND_RRESP:begin
                axi_done    <= 1'b0;
                axi_state   <= (s_axi_rvalid && s_axi_rready)?AXI_IDLE:SEND_RRESP; 
            end
            RESET:begin
                reset_count <= reset_count + 1'b1;
                axi_state   <= (reset_count == 0)?axi_next_state:axi_state; 
            end
            default: begin
                axi_state      <= AXI_IDLE;
                axi_next_state <= AXI_IDLE; 
            end
        endcase
    end
end
// axi side operations ====================ENDS============================
always@(posedge axi_aclk)begin
    if(!axi_arstn)begin
        phase_var   <= 0;
        sccb_state  <= IDLE;
        phase_index <= 0;
        clk_count   <= 0; 
    end
    else begin
        case(sccb_state)
            IDLE:begin
                sccb_state  <= (axi_done)?START:IDLE;
                sccb_start  <=  1'b1;
                phase_index <= 0;
                clk_count   <= 0;
                phase_var   <= 3'd1;
            end
            START:begin
                sda_reg         <= 1'b0; // setting sda low while scl is high.It means transaction will occur 
                sccb_state      <= DELAY;
                sccb_next_state <= DROP_SCL;
            end
            DELAY:begin // 2000ns delay creation.It is necessary for SCL and SDA signal stability.
                clk_count      <= clk_count + 1'b1;
                sccb_state     <= (clk_count == PREDEF_CLK_DELAY)?sccb_next_state:sccb_state;
            end
            DROP_SCL:begin // dropping scl and starting it for transaction phase
                sck_start       <= 1'b1;
                sccb_state      <= DELAY;
                sccb_next_state <= SEND; 
                clk_count       <= 0;
            end
            SEND:begin // it sends given array of bits in this state. 3 phase for write and 4 phase for read operations.
                if(count_bit == 4'd0)begin
                    if(((phase_var+1'b1) == PHASE_THREE)&&(!read_op_inprogress) )     count_bit <= 4'd9;
                    else if((phase_var+1'b1) == PHASE_FIVE) count_bit <= 4'd9;
                    else                             count_bit <= 4'd8;
                end
                else count_bit <= count_bit - 1'b1;
                case(phase_var)
                    PHASE_ONE:begin
                        sda_reg <= wr_id_address[count_bit];
                    end
                    PHASE_TWO:begin
                        sda_reg <= sub_address[count_bit];
                    end
                    PHASE_THREE:begin
                        sda_reg <= write_data[count_bit];
                    end
                    PHASE_FOUR:begin
                        sda_reg <= rd_id_address[count_bit];
                    end
                    PHASE_FIVE:begin
                        sda_reg              <= read_phs[count_bit];
                        read_data[count_bit] <= SDA;
                    end
                    default:begin
                        sda_reg <= 1'bz;
                    end
                endcase
                sccb_state <= HIGH_SCL;
                sccb_start <= !( ( (phase_var == 3'd3) | (phase_var == 3'd5) )&&(count_bit == 4'd0) ); //if this is set to 0, this means operation done so gradually move towards idle state
                if( (phase_var == PHASE_TWO) && (read_op_inprogress) )begin
                    phase_var <= (count_bit == 4'd0)?PHASE_FOUR:(phase_var);
                end
                else begin
                    phase_var <= (count_bit == 4'd0)?(phase_var + 1'b1):(phase_var);
                end
                clk_count <= 0;
            end
            HIGH_SCL:begin
                if(SCL)begin
                    sccb_state      <= (!sccb_start)?DELAY:LOW_SCL;
                    sccb_next_state <= (!sccb_start)?HIGH_SDA:DELAY;
                end
                else begin
                    sccb_state <= HIGH_SCL;
                end
            end
            LOW_SCL:begin
                sccb_state <= (SCL !=1'b0)?LOW_SCL:DELAY;
                sccb_next_state <= SEND; 
                clk_count  <= 0;
            end
            HIGH_SDA:begin
                sda_reg         <= 1'b1;
                sck_start       <= 1'b0;
                sccb_state      <= DELAY;
                sccb_next_state <= IDLE;
                phase_index     <= 0;
                clk_count       <= 0;
            end
            default:begin
                sccb_state      <= IDLE;
                sccb_next_state <= IDLE;
            end
        endcase
    end
end

assign SCL  = sck_reg;
assign SDA  = (sccb_state == IDLE)?1'b1:sda_reg; 
assign PWDN = 1'b0;
assign RST  = (axi_state == RESET)?1'b0:1'b1;

endmodule